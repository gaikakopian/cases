$(document).ready(function(){
    
    $( ".case_items" ).sortable();
    
    
    $('.save_items_order').on('click', function(){
        var data = [];
        
        $(".item-dragable").each(function(key, v) {
            data.push({
                id: $(this).attr('junction'),
                value: key
            });
        });
        
        $.ajax({
            url: App.base_path + 'ajax/save-case-items-order',
            type: 'POST',
            data: {
                data: data
            },
            success: function (response) {
                if (response)
                    alert('Сохранено');
            }
        });
    });
    
    $('#quality-search').on('change' , function(){
        var This = $(this);
        var category = This.val();

        var items = $('.market__item-profile').not('.item-dragable');

        if (category == 'all') {
            items.show();
            return false;
        }

        $.each(items , function(index , value){
            if ($(value).attr('category') == category) {
                $(value).show();
            } else {
                $(value).hide();
            }

        });
    });
    
    $(document).on('click', '.remove_item', function(){

        var This = $(this);
        var id = $(this).closest('.market__item').attr('item');
        
        $.ajax({
            url: App.base_path + 'ajax/remove-case-item',
            type: 'POST',
            data: {
                item_id: id,
                case_id: $('.case_items ').attr('case_id')
            },
            success: function (response) {
                if(response) {
                    This.closest('.market__item').find('remove_item').addClass('add_item').removeClass('remove_item').html('+');
                    var element = This.closest('.market__item').clone(true);
                    var attr = This.closest('.market__item').attr('category');
                    if (typeof attr !== typeof undefined && attr !== false){
                         $('#freecase_items').append(element);
                    } else {
                        $('#outcase_items').append(element);
                    }
                    This.closest('.market__item').remove();
                }
            }
        });
    });

    $(document).on('click', '.add_item', function(){

        var This = $(this);
        var item = $(this).closest('.market__item').attr('item');
        var case_id = $(this).closest('.free_items').attr('case_id');
        
        $.ajax({
            url: App.base_path + 'ajax/add-case-item',
            type: 'POST',
            data: {
                item_id: item,
                case_id: case_id
            },
            success: function (data) {

                if(data.status == 'success') {
                    
                    This.closest('.market__item').find('.add_item').addClass('remove_item').removeClass('add_item').html('X');
                    var element = This.closest('.market__item').clone(true);
                    This.closest('.market__item').remove();
                    $('.case_items').append(element);
                }
            }
        });                
    });
    
});