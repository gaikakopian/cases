<?php
namespace backend\controllers;

use Yii;
use backend\controllers\BaseController;
use common\models\Bots;
use common\models\Items;
use common\models\ItemsList;
use common\models\CaseItems;

class BotsController extends BaseController
{

    public function actionIndex(){
        
        $model = new Bots();
        
        return $this->render('index', [
            'searchModel' => $model,
            'dataProvider' => $model->search()
        ]);
    }
    
    public function actionGetItemInfo() {

        if(Yii::$app->request->isAjax) {
            
            if(Yii::$app->request->post('name')) {  
                $name = trim(Yii::$app->request->post('name'));
                
                $bot = Bots::getRandomBot();
                
                $info = json_decode(@file_get_contents("https://market.csgo.com/api/ItemInfo/{$name}/en/?key={$bot->secret_key}") , true);
                if (array_key_exists('error', $info) && $info['error'] == 'Bad KEY')
                    return false;
                
                $req_mhn = rawurlencode($info['market_hash_name']);
                
                $items = file_get_contents("https://market.csgo.com/api/SearchItemByName/{$req_mhn}/?key={$bot->secret_key}");
                
                if(!$items)
                   return false;

                $item_data = json_decode($items , true);
                
                if (!$item_data['success'])
                    return false;
                
                return json_encode($item_data['list']);
            }
        }
    }
    
    public function actionGetItem() {
        
        if (Yii::$app->request->isAjax) {
            
            if (Yii::$app->request->post('name') && Yii::$app->request->post('title') && Yii::$app->request->post('case_id')) {
                $name = trim(Yii::$app->request->post('name'));
                $title = trim(Yii::$app->request->post('title'));
                $case_id = trim(Yii::$app->request->post('case_id'));
                $class_id = trim(Yii::$app->request->post('class_id'));
                $quality = trim(Yii::$app->request->post('quality'));
                
                $bot = Bots::getRandomBot();
                
                $image_name = md5("item_" . $class_id . microtime());
                $image = $image_name . '.png';
                
                if (!copy("https://cdn.csgo.com/item/" . urlencode($quality) . "/300.png", \Yii::getAlias('@frontend')."/web/images/items/" . $image))
                    return json_encode (['success' => false , 'status' => 'Ошибка']);
                
                $info = json_decode(file_get_contents("https://csgo.tm/api/ItemInfo/{$class_id}/ru/?key={$bot->secret_key}"), true);
                if (strpos($info['name'], 'StatTrak') !== false) {
                    return json_encode (['success' => false , 'status' => 'Предмет уже добавлен']);
                }
                
                if (isset($info['classid'])) {
                    
                    $rarity = Items::getCategory($info['rarity']);
                    $market_hash_name = $info['market_hash_name'];
                    $market_name = explode(' (', $market_hash_name);
                    $class_id = $info['classid'];
                    $instance_id = $info['instanceid'];
                }
                
                $steam_item = json_decode(@file_get_contents('http://steamcommunity.com/market/priceoverview/?currency=5&appid=730&market_hash_name='.rawurlencode($market_hash_name)) , true);
                
                if (!$steam_item || !$steam_item['success'] || !array_key_exists('median_price', $steam_item))
                    $steam_item = array('median_price' => ($info['offers'][0]['price']/100).' руб');
                
                $item_check = Items::findOne(['market_hash_name' => $market_name[0]]);
                
                if ($item_check)
                    return json_encode (['success' => false , 'status' => 'Предмет уже добавлен']);
                
                $arguments = array($name,
                    $title,
                    $info['name'],
                    $market_name[0], 
                    $bot->id ,
                    $class_id,
                    $instance_id,
                    $class_id.'_'.$instance_id ,
                    $market_name[0],
                    $market_hash_name,
                    $image,
                    $info['offers'][0]['price']/100,
                    $steam_item['median_price'],
                    $rarity,
                );
                
                $item = Items::Create($arguments);
                
                if (!$item)
                    return json_encode (['success' => false , 'status' => 'Ошибка']);
                
                if (CaseItems::getPair($case_id, $item->id)) 
                    return json_encode(['success' => true]);
                
                $caseitems = CaseItems::Create($case_id, $item->id);
                
                if ($caseitems)
                    return json_encode (['success' => true]);
            } 
        }
    }
    
    public function actionBuyItem() {

        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->post('item_id') && Yii::$app->request->post('bot_id')) {
                $category = Yii::$app->request->post('category') != '' ? Yii::$app->request->post('category') : null;
                return $this->buy(Yii::$app->request->post('item_id'), Yii::$app->request->post('bot_id') , null , $category);
            } else {
                return false;
            }
        }
    }
    
    public function actionBuyStatItem() {

        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->post('item_id') && Yii::$app->request->post('bot_id')) {
                return $this->buy(Yii::$app->request->post('item_id'), Yii::$app->request->post('bot_id') , Yii::$app->request->post('quality'));
            } else {
                return false;
            }
        }
    }
    
    private function buy($item_id = null, $bot_id = null, $quality = null, $category = null) {
        
        $item = Items::find()->select('items.id as id, items.name as name, items.title as title, sum(items_list.price)/count(items_list.id) as min_price , items.mhn_steam as mhn , items.market_hash_name')
                ->leftJoin('items_list', 'items_list.item_id = items.id')
                ->where(['items.id' => $item_id])
                ->asArray()
                ->one();
        
        $bot = Bots::findOne($bot_id)->getAttributes();
        
        $networkId = 0;
        
        if($item['mhn']){
            if (!$quality){
                if (!$category){
                    $body = rawurlencode($item['mhn']);
                }else{
                    rawurlencode($item['market_hash_name'].' ('.$category.')');
                }
                
            } else {
                $body = rawurlencode('StatTrak™ '.$item['market_hash_name'].' ('.$quality.')');
            }
            
            $item_data_site = @file_get_contents("https://market.csgo.com/api/SearchItemByName/{$body}/?key={$bot['secret_key']}");    

            if($item_data_site === false){
                return false;
            }

            $item_data = json_decode($item_data_site , true);
            
            if($item_data){
                if(count($item_data['list'])){
                    if (!$quality){
                        if($item_data['list'][0]){
                            $networkId = $item_data['list'][0]['i_classid'].'_'.$item_data['list'][0]['i_instanceid'];
                        }
                    } else {
                        foreach ($item_data['list'] as $item){
                            if (strpos($item['name'], 'StatTrak') !== false) {
                                $networkId = $item['i_classid'].'_'.$item['i_instanceid'];
                                break;
                            }
                        }
                    }
                }
            }
        }
        
        if (!$networkId) {
            return 'Нет Предметов';
        }

        $check_request = json_decode(file_get_contents("https://csgo.tm/api/ItemInfo/{$networkId}/ru/?key={$bot['secret_key']}"), true);

        if (!empty($check_request['error'])) {
            return $check_request['error'];
        }

        if (!empty($item['min_price']) && ($check_request['min_price'] / 100) > ($item['min_price'] + 500)) {
            return 'Предмет очень дорогой';
            exit;
        }

        $buy_request = json_decode(file_get_contents("https://csgo.tm/api/Buy/{$networkId}/{$check_request['min_price']}/{$check_request['hash']}/?key={$bot['secret_key']}"), true);
        
        if (isset($buy_request['result']) && $buy_request['result'] == 'ok') {

            $amount = ceil($check_request['min_price']/100);
            
            $steam_item = json_decode(@file_get_contents('http://steamcommunity.com/market/priceoverview/?currency=5&appid=730&market_hash_name='.rawurlencode($check_request['market_hash_name'])) , true);
            
            if (!$steam_item || !$steam_item['success'] || !array_key_exists('median_price', $steam_item))
                $steam_item = array('median_price' => ($amount).' руб');
            
            $new_steam_price = substr(str_replace(',', '.', $steam_item['median_price']), 0, -7);
            
            $items_list = ItemsList::findAll(['message' => $check_request['market_hash_name'] , 'status' => 'in_stock']);
            
            foreach ($items_list as $item){
                $item->steam_price = $new_steam_price;
                $item->save(false);
            }
            
            $model = new ItemsList();
            
            $model->setAttributes(
                    [
                    'item_id' => $item_id,
                    'price' => $amount,
                    'steam_price' => $new_steam_price,
                    'trade_id' => $buy_request['id'],
                    'bot_id' => $bot_id,
                    'quality' => Items::getQuality($check_request['quality']),
                    'created' => date('Y-m-d H:i:s'),
                    'status' => 'pending'
                    ], false);
            
            $model->save(false);
            return true;
        } else {
            return $buy_request['result'];
        }
    }

}
